ASM=nasm
ASMFLAGS=-g -felf64 -o
LD=ld
LDFLAGS=-o

program: main.o lib.o dict.o
	$(LD) $(LDFLAGS) $@ $^

main.o: main.asm colon.inc words.inc lib.inc dict.inc
	$(ASM) $(ASMFLAGS) $@ $<

lib.o: lib.asm 
	$(ASM) $(ASMFLAGS) $@ $<

dict.o: dict.asm dict.inc
	$(ASM) $(ASMFLAGS) $@ $<

python: program
	python3 test.py

.PHONY: clean
clean:
	rm -rf *.o