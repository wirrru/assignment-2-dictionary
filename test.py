import subprocess

keys = ["one", 
        "two",
        "three", 
        "", 
        "dlkfsjdihisvnsnvklshvureroiejfkldfbhsuhiajldmnfbsfieosnvhsiajwoldmnjskfhjelakdscmdfbjkghfiejwoda;lmkfjgkerhjfiwklejgekrwfialdkfweijawdlkfsjdihisvnsnvklshvureroiejfkldfbhsuhiajldmnfbsfieosnvhsiajwoldmnjskfhjelakdscmdfbjkghfiejwoda;lmkfjgkerhjfiwklejgekrwfialdkfweijaw"]
expected_out = ["Don't pick up the phone",
                "Don't let him in, you'll have to kick him out again", 
                "Don't be his friend",  
                "", 
                ""]
expected_err = ["", 
                "", 
                "", 
                "Error: not found", 
                "Error: max length is 255"]

passed = 0
for i in range(len(keys)):
    pr = subprocess.Popen(['./program'], stdin = subprocess.PIPE, stdout = subprocess.PIPE, stderr = subprocess.PIPE)
    stdout, stderr = pr.communicate(input=keys[i].encode())
    out_result, err_result = stdout.decode().strip(), stderr.decode().strip()
    if (out_result == expected_out[i] and err_result == expected_err[i]):
        passed =  passed + 1
        print("Test " + str(i+1) + " passed\n")
    else:
        print("Test "+str(i+1)+":\n")
        if(out_result != expected_out[i]):
            print("out_result should be:" + expected_out[i] + "\n")
        if(err_result != expected_err[i]):
            print("err_result should be:" + expected_err[i] + "\n")
print("Tests " + str(passed) + "/5 passed")

