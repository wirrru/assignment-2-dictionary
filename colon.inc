%define current_point 0

%macro colon 2

%%next:
dq current_point
%define current_point %%next
db %1, 0

%2:
%endmacro
