%include "colon.inc"
%include "words.inc"
%include "lib.inc"
%include "dict.inc"

global _start
extern find_word


section .rodata
hello:    db "Enter key: ", 0
size_error: db "Error: max length is 255", 0xA, 0
name_error: db "Error: not found", 0xA, 0

section .bss

buffer: resb 256;

section .text


_start:
    mov rdi, hello
    call print_string
    mov rdi, buffer
    mov rsi, 255
    call read_word
    test rax, rax
    je .s_error
    mov rdi, buffer
    mov rsi, current_point
    call find_word
    test rax, rax
    je .n_error
    add rax, 8
    mov rdi, rax
    push rdi
    call string_length
    pop rdi
    inc rdi
    add rdi, rax
    call print_string
    call print_newline
    call exit

.n_error:
    mov rdi, name_error
    call print_error
    jmp .end
.s_error:
    mov rdi, size_error
    call print_error
.end:
    call exit
