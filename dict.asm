%include "dict.inc"
section .text
global find_word

find_word:
    xor rax, rax
	push r12
	push r13
	mov r12, rdi
	mov r13, rsi
.loop:
	test rsi, rsi
	jz .fail
	add rsi, 8
	call string_equals
    test rax, rax
    jnz .success
	mov rsi, [r13]
	mov r13, rsi
    jmp .loop
.success:
	mov rax, r13
.fail:
	pop r13
    pop r12
	ret
	